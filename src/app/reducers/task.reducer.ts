import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as TaskActions from '../actions/task.actions';
import { Filters, Task } from '../models/task.model';

export const tasksFeatureKey = 'tasks';

export interface State extends EntityState<Task> {
  loading: boolean,
  error: string,
  filters: Filters
}

export const adapter: EntityAdapter<Task> = createEntityAdapter<Task>();

export const initialState: State = adapter.getInitialState({
  loading: false,
  error: null,
  filters: {
    category: null,
    done: true,
    text: null,
  }
});


export const reducer = createReducer(
  initialState,
  on(TaskActions.loadTasks, (state) => adapter.removeAll(
    {...state, loading: true, error: null}
  )),
  on(TaskActions.loadTasksSuccess,
    (state, action) => adapter.setAll(action.tasks, {
      ...state,
      loading: false,
      error: null
    })
  ),
  on(TaskActions.loadTasksFailure, (state, {error}) => (
    {
      ...state, 
      loading: false,
      error
    }
  )),
  on(TaskActions.addTaskSuccess,
    (state, action) => adapter.addOne(action.task, state)
  ),
  on(TaskActions.upsertTask,
    (state, action) => adapter.upsertOne(action.task, state)
  ),
  on(TaskActions.addTasks,
    (state, action) => adapter.addMany(action.tasks, state)
  ),
  on(TaskActions.upsertTasks,
    (state, action) => adapter.upsertMany(action.tasks, state)
  ),
  on(TaskActions.updateTaskSuccess,
    (state, action) => adapter.updateOne(
      {
        id: action.task.id,
        changes: action.task
      }, 
      state)
  ),
  on(TaskActions.updateTasks,
    (state, action) => adapter.updateMany(action.tasks, state)
  ),
  on(TaskActions.deleteTaskSuccess,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(TaskActions.deleteTasks,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(TaskActions.clearTasks,
    state => adapter.removeAll(state)
  ),
  on(TaskActions.setFilters, (state, {filters}) => (
    {
      ...state, 
      filters
    }
  )),
  on(TaskActions.clearFilters, (state) => (
    {
      ...state, 
      filters: Object.assign({}, initialState.filters)
    }
  )),
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();