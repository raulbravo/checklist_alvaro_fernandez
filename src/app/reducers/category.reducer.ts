import { createReducer, on } from '@ngrx/store';
import * as CategoryActions from '../actions/category.actions';
import { Category } from '../models/task.model';

export const categoryFeatureKey = 'category';

export interface State {
  categories: Category[];
  loading: boolean;
  error: string;
}

export const initialState: State = {
  categories: null,
  loading: null,
  error: null,
};


export const reducer = createReducer(
  initialState,

  on(CategoryActions.loadCategories, state => ({...state, loading: true, error: null})),
  on(CategoryActions.loadCategoriesSuccess, (state, action) => ({
    ...state,
    loading: false,
    categories: action.categories
  })),
  on(CategoryActions.loadCategoriesFailure, (state, action) => ({...state, loading: false, error: action.error})),

);

