import { createReducer, on } from '@ngrx/store';
import * as TagActions from '../actions/tag.actions';
import { Tag } from '../models/task.model';

export const tagFeatureKey = 'tag';

export interface State {
  tags: Tag[];
  loading: boolean;
  error: string;
}

export const initialState: State = {
  tags: null,
  loading: null,
  error: null,
};


export const reducer = createReducer(
  initialState,

  on(TagActions.loadTags, state => ({...state, loading: true, error: null})),
  on(TagActions.loadTagsSuccess, (state, action) => ({
    ...state,
    loading: false,
    tags: action.tags
  })),
  on(TagActions.loadTagsFailure, (state, action) => ({...state, loading: false, error: action.error})),

);

