import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { categoryActions, taskActions, tagActions } from './actions'
import { EditTaskDialogComponent } from './components/edit-task-dialog.component';
import { NewTaskDialogComponent } from './components/new-task-dialog.component';
import { Category, Filters, Tag, Task } from './models/task.model';
import { taskSelectors, categorySelectors, tagSelectors } from './selectors/indext'
 
@Component({
  selector: 'app-root',
  template: `
  <ng-container *ngIf="categoryList$ |async as categories">
    <ng-container *ngIf="tagList$ |async as tags">
      <app-toolbar
        (addTask)="addTask(categories, tags)"
      ></app-toolbar>
      <app-filters
      [filters]="filters$|async"
      [categories]="categories"
      [tags]="tags"
      (filter)="applyFilters($event)"
      ></app-filters>
      <app-task-list
        *ngIf="taskList$ | async as tasks"
        [tasks]="tasks"
        (editTask)="editTask($event, categories, tags)"
        (deleteTask)="deleteTask($event)"
        (swithcTaskStatus)="switchTask($event)"
        (clearFilters)="clearActiveFilters()"
      ></app-task-list>
    </ng-container>
  </ng-container>

  <ng-container *ngIf="taskLoading$ | async as tasks">
    <p>Loading tasks...</p>
  </ng-container>
  <ng-container *ngIf="taskError$ | async as error">
    <button (click)="loadFilters()">Reload filters</button>
    <pre><p style="color: red">Error: {{error | json}}</p></pre>
  </ng-container>

  <ng-container *ngIf="categoryLoading$ | async as tasks">
    <p>Loading categories...</p>
  </ng-container>
  <ng-container *ngIf="categoryError$ | async as error">
    <button (click)="loadFilters()">Reload filters</button>
    <pre><p style="color: red">Error: {{error | json}}</p></pre>
  </ng-container>
  <ng-container *ngIf="taskLoading$ | async as tasks">
    <p>Loading tags...</p>
  </ng-container>
  <ng-container *ngIf="taskError$ | async as error">
    <button (click)="loadTasks()">Reload tasks</button>
    <pre><p style="color: red">Error: {{error | json}}</p></pre>
  </ng-container>
  `
})
export class AppComponent {

  taskLoading$: Observable<boolean> = this.store.select(taskSelectors.taskLoading);
  taskError$: Observable<string> = this.store.select(taskSelectors.taskError);
  taskList$: Observable<Task[]> = this.store.select(taskSelectors.getFilteredTaskList);

  filters$: Observable<Filters> = this.store.select(taskSelectors.getActiveFilters);

  categoryLoading$: Observable<boolean> = this.store.select(categorySelectors.categoryLoading);
  categoryError$: Observable<string> = this.store.select(categorySelectors.categoryError);
  categoryList$: Observable<Category[]> = this.store.select(categorySelectors.getCategoryList);

  tagLoading$: Observable<boolean> = this.store.select(tagSelectors.tagLoading);
  tagError$: Observable<string> = this.store.select(tagSelectors.tagError);
  tagList$: Observable<Tag[]> = this.store.select(tagSelectors.getTagList);

  constructor(
    private store: Store,
    private dialog: MatDialog
  ) {
    this.loadTasks();
    this.loadFilters();
  }

  loadTasks() {
    this.store.dispatch(taskActions.loadTasks())
  }

  addTask(categories: Category[], tags: Tag[]) {
    const addTaskDialogRef = this.dialog.open(NewTaskDialogComponent, {
      data: {
        categories,
        tags
      }
    });
    addTaskDialogRef.afterClosed().subscribe(task => {
      this.store.dispatch(taskActions.addTask({task}))
    });
  }

  editTask(task: Task, categories: Category[], tags: Tag[]) {
    const addTaskDialogRef = this.dialog.open(EditTaskDialogComponent, {
      data: {
        task,
        categories,
        tags
      }
    });
    addTaskDialogRef.afterClosed().subscribe(task => {
      this.store.dispatch(taskActions.updateTask({task}))
    });
  }

  switchTask(task: Partial<Task>) {
    this.store.dispatch(taskActions.updateTask({task}))
  }

  deleteTask(id: string) {
      this.store.dispatch(taskActions.deleteTask({id}))
  }


  loadFilters() {
    this.store.dispatch(categoryActions.loadCategories());
    this.store.dispatch(tagActions.loadTags())
  }

  applyFilters(filters: Filters) {
    this.store.dispatch(taskActions.setFilters({filters}))
  }
  clearActiveFilters() {
    this.store.dispatch(taskActions.clearFilters())
  }
}
