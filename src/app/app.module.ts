import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { EditTaskDialogComponent } from './components/edit-task-dialog.component';
import { FiltersComponent } from './components/filters.component';
import { NewTaskDialogComponent } from './components/new-task-dialog.component';
import { TaskListComponent } from './components/task-list.component';
import { ToolbarComponent } from './components/toolbar.component';
import { AppEffects } from './effects/app.effects';
import { CategoryEffects } from './effects/category.effects';
import { TagEffects } from './effects/tag.effects';
import { TaskEffects } from './effects/task.effects';
import { MaterialModule } from './material.module';
import * as fromCategory from './reducers/category.reducer';
import * as fromTag from './reducers/tag.reducer';
import * as fromTask from './reducers/task.reducer';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    FiltersComponent,
    TaskListComponent,
    NewTaskDialogComponent,
    EditTaskDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    StoreModule.forFeature(fromTask.tasksFeatureKey, fromTask.reducer),
    StoreModule.forFeature(fromCategory.categoryFeatureKey, fromCategory.reducer),
    StoreModule.forFeature(fromTag.tagFeatureKey, fromTag.reducer),
    EffectsModule.forRoot([AppEffects]),
    StoreModule.forRoot({}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forFeature([TaskEffects, CategoryEffects, TagEffects]),
    BrowserAnimationsModule,
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
