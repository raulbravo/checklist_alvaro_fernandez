import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Category } from '../models/task.model';
// import { ErrorsHelper } from '@core/utils';

@Injectable({providedIn: 'root'})
export class CategoriesService {
  constructor(
    private http: HttpClient,
    // private errorsHelper: ErrorsHelper
  ) { }

  getCategoriesList(): Observable<Category[]> {
    return this.http.get<Category[]>('https://checklistbackend.azurewebsites.net/api/categories')/*.pipe(
      map((response: Category[]) => {debugger; return response})
      // catchError(error => this.errorsHelper.genericResponse(error, 'error-getting-materials-list'))
    );*/
  }

}
