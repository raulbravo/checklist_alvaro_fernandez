import { HttpClient, HttpStatusCode } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Update } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({providedIn: 'root'})
export class TasksService {
  constructor(
    private http: HttpClient,
  ) { }

  getTasksList(): Observable<Task[]> {
    return this.http.get<Task[]>('https://checklistbackend.azurewebsites.net/api/tasks')
  }

  addTask(task: Partial<Task>): Observable<Task> {
    const newTask = {
      text: task.text,
      categoryId: task.category
    }
    return this.http.post<Task>('https://checklistbackend.azurewebsites.net/api/tasks', newTask)
  }

  updateTask(task: Partial<Task>): Observable<Task> {
    let editedTask = new Array;
    
    if (task.done === true) {
      editedTask.push({
        op: 'replace',
        path: 'done',
        value: task.done
      })
    } else if (task.done === false) {
      editedTask.push({
        op: 'replace',
        path: 'done',
        value: task.done
      })
    }
    if (task.text) {
      editedTask.push({
        op: 'replace',
        path: 'text',
        value: task.text
      })
    }
    if(task.category) {
      editedTask.push({ // TODO edited correctly but call returns null
        op: 'replace',
        path: 'categoryId',
        value: task.category
      })
    }

    return this.http.patch<Task>('https://checklistbackend.azurewebsites.net/api/tasks/'+task.id, editedTask)
  }

  deleteTask(taskId: string): Observable<Task> {
    return this.http.delete<Task>('https://checklistbackend.azurewebsites.net/api/tasks/'+taskId)
  }
}
