import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Tag } from '../models/task.model';
// import { ErrorsHelper } from '@core/utils';

@Injectable({providedIn: 'root'})
export class TagsService {
  constructor(
    private http: HttpClient,
    // private errorsHelper: ErrorsHelper
  ) { }

  getTagsList(): Observable<Tag[]> {
    return this.http.get<Tag[]>('https://checklistbackend.azurewebsites.net/api/tags')/*.pipe(
      map((response: Tag[]) => {debugger; return response})
      // catchError(error => this.errorsHelper.genericResponse(error, 'error-getting-materials-list'))
    );*/
  }

}
