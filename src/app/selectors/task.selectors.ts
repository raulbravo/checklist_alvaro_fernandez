import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTask from '../reducers/task.reducer';

export const selectTaskState = createFeatureSelector<fromTask.State>(
    fromTask.tasksFeatureKey
);

export const getTaskList = createSelector(
  selectTaskState,
  fromTask.selectAll
);

export const taskLoading = createSelector(
    selectTaskState,
    (state) => state.loading
);

export const taskError = createSelector(
    selectTaskState,
    (state) => state.error
);

export const getActiveFilters = createSelector(
    selectTaskState,
    (state) => state.filters
);

export const getFilteredTaskList = createSelector (
    getTaskList,
    getActiveFilters,
    (tasks, filters) => {
        let fitleredTasks = tasks;
        if (filters.done===false) {
            fitleredTasks = fitleredTasks.filter(t => t.done === false)
        }
        if (filters.category) {
            fitleredTasks = fitleredTasks.filter(t => t.category.id === filters.category)
        }
        if (filters.text) {
            fitleredTasks = fitleredTasks.filter(t => t.text.includes(filters.text))
        }
        return fitleredTasks
    }
);