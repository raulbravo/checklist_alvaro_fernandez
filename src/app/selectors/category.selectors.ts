import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCategory from '../reducers/category.reducer';

export const selectCategoryState = createFeatureSelector<fromCategory.State>(
  fromCategory.categoryFeatureKey
);

export const getCategoryList = createSelector(
  selectCategoryState,
  (state) => state.categories
);

export const categoryLoading = createSelector(
    selectCategoryState,
    (state) => state.loading
);

export const categoryError = createSelector(
    selectCategoryState,
    (state) => state.error
);