import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTag from '../reducers/tag.reducer';

export const selectTagState = createFeatureSelector<fromTag.State>(
  fromTag.tagFeatureKey
);

export const getTagList = createSelector(
  selectTagState,
  (state) => state.tags
);

export const tagLoading = createSelector(
    selectTagState,
    (state) => state.loading
);

export const tagError = createSelector(
    selectTagState,
    (state) => state.error
);