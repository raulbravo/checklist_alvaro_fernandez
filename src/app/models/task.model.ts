export interface Category {
  id: string;
  name: string
}

export interface Task {
  id: string;
  category: Category;
  done: boolean;
  sortNumber: number;
  subTasks: SubTask[];
  tags: Tag[];
  text: string
}
export interface SubTask {
  id: string;
  done: boolean;
  sortNumber: number;
  text: string
}
export interface Tag {
  id: string;
  name: string
}
export interface Category {
  id: string;
  name: string
}

export interface Filters {
  category: string;
  done: boolean;
  text: string;
}
