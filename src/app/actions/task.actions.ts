import { HttpStatusCode } from '@angular/common/http';
import { Update } from '@ngrx/entity';
import { createAction, props } from '@ngrx/store';
import { Filters, Task } from '../models/task.model';


export const loadTasks = createAction(
  '[Task/API] Load Tasks'
);
export const loadTasksSuccess = createAction(
  '[Task/API] Load Tasks Success', 
  props<{ tasks: Task[] }>()
);
export const loadTasksFailure = createAction(
  '[Task/API] Load Tasks Failure', 
  props<{ error: string }>()
);

export const addTask = createAction(
  '[Task/API] Add Task',
  props<{ task: Partial<Task> }>()
);

export const addTaskSuccess = createAction(
  '[Task/API] Add Task Sucess',
  props<{ task: Task }>()
);

export const addTaskFailure = createAction(
  '[Task/API] Add Task Failure',
  props<{ error: string }>()
);

export const upsertTask = createAction(
  '[Task/API] Upsert Task',
  props<{ task: Task }>()
);

export const addTasks = createAction(
  '[Task/API] Add Tasks',
  props<{ tasks: Task[] }>()
);

export const upsertTasks = createAction(
  '[Task/API] Upsert Tasks',
  props<{ tasks: Task[] }>()
);

export const updateTask = createAction(
  '[Task/API] Update Task',
  props<{ task: Partial<Task> }>()
);

export const updateTaskSuccess = createAction(
  '[Task/API] Update Task Sucess',
  props<{ task: Partial<Task> }>()
);

export const updateTaskFailure = createAction(
  '[Task/API] Update Task Failure',
  props<{ error: string }>()
);


export const updateTasks = createAction(
  '[Task/API] Update Tasks',
  props<{ tasks: Update<Task>[] }>()
);

export const deleteTask = createAction(
  '[Task/API] Delete Task',
  props<{ id: string }>()
);

export const deleteTaskSuccess = createAction(
  '[Task/API] Delete Task Success',
  props<{ id: string }>()
);

export const deleteTaskFailure = createAction(
  '[Task/API] Delete Task Failure',
  props<{ error: string }>()
);

export const deleteTasks = createAction(
  '[Task/API] Delete Tasks',
  props<{ ids: string[] }>()
);

export const clearTasks = createAction(
  '[Task/API] Clear Tasks'
);

export const setFilters = createAction(
  '[Task] Set Filters',
  props<{ filters: Filters }>()
);

export const clearFilters = createAction(
  '[Task] Clear Filters'
);