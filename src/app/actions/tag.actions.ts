import { createAction, props } from '@ngrx/store';
import { Tag } from '../models/task.model';

export const loadTags = createAction(
  '[Tag] Load Tags'
);

export const loadTagsSuccess = createAction(
  '[Tag] Load Tags Success',
  props<{ tags: Tag[] }>()
);

export const loadTagsFailure = createAction(
  '[Tag] Load Tags Failure',
  props<{ error: any }>()
);
