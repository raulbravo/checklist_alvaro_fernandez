import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Category, Tag, Filters } from '../models/task.model';

@Component({
  selector: 'app-filters',
  template: `
    <form [formGroup]="form" class="container">
      <mat-form-field appearance="outline" *ngIf="categories && categories.length" class="item">
        <mat-label>Filter by Category</mat-label>
        <mat-select formControlName="category">
          <mat-option *ngFor="let category of categories" [value]="category.id">
            {{category.name}}
          </mat-option>
        </mat-select>
      </mat-form-field>
      
      <mat-form-field appearance="outline" *ngIf="tags && tags.length" class="item">
        <mat-label>Filter by Tags</mat-label>
        <mat-select formControlName="tag" >
          <mat-option *ngFor="let tag of tags" [value]="tag.id">
            {{tag.name}}
          </mat-option>
        </mat-select>
      </mat-form-field>

      <div class="item container">
        <mat-form-field appearance="outline" class="item">
          <mat-label>Filter by Text</mat-label>
          <input matInput formControlName="text">
        </mat-form-field>

        <mat-checkbox class="item" formControlName="done">Show done tasks</mat-checkbox>
      </div>
    </form>
  `,
  styles: [`
    .container{        
      display: flex;
      flex-wrap: wrap;
      justify-content: space-around;
      column-gap: 10px;
    }
    .item {
      flex-grow: 1
    }
  `]
})
export class FiltersComponent {
  @Input() set filters(filters: Filters) {
    this.setFilters(filters)
  }
  @Input() categories: Category[];
  @Input() tags: Tag[];

  @Output() filter = new EventEmitter<Filters>();

  form = new FormGroup({
    category: new FormControl(null),
    tag: new FormControl({value: null, disabled: true},),
    text: new FormControl(null),
    done: new FormControl(true),
  });

  constructor() {
    this.form.valueChanges.subscribe(d => {
      this.filter.next(this.form.value)
    })
  }

  setFilters(filters: Filters) {
    this.form.patchValue(filters)
  }
}
