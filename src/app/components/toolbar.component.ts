import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  template: `
    <mat-toolbar color="primary">
      <span>CheckList</span>
      <span>
        <button mat-icon-button>
          <mat-icon>settings</mat-icon>
        </button>
        <button (click)="addTask.next()" mat-icon-button>
          <mat-icon>add</mat-icon>
        </button>
      </span>
    </mat-toolbar>
  `,
  styles: [`
    .mat-toolbar {
      color: white;
      display: flex;
      justify-content: space-between;
    }
  `]
})
export class ToolbarComponent {
  @Output() addTask = new EventEmitter();
}
