import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category, Tag, Task } from '../models/task.model';

@Component({
    template: `
    <h1 mat-dialog-title>Edit Task</h1>
    <div mat-dialog-content>
        <form  [formGroup]="form">
        <mat-form-field appearance="outline" class="item">
          <mat-label>Task Description</mat-label>
          <input matInput formControlName="text">
        </mat-form-field>

        <mat-form-field appearance="outline" *ngIf="data.categories && data.categories.length" class="item">
            <mat-label>Category</mat-label>
            <mat-select formControlName="category">
            <mat-option *ngFor="let category of data.categories" [value]="category.id">
                {{category.name}}
            </mat-option>
            </mat-select>
        </mat-form-field>
        
        </form>
        {{data.task | json}}
    </div>
    <div mat-dialog-actions>
    <button mat-button (click)="cancel()">Cancel</button>
    <button mat-button [mat-dialog-close]="updatedTask(form.value)" [disabled]="form.invalid" cdkFocusInitial>Save</button>
    </div>
    `
  })

export class EditTaskDialogComponent {

    form = new FormGroup({
        text: new FormControl('', Validators.required),
        category: new FormControl(''),
        // tags: TODO
    });

  constructor(
    public dialogRef: MatDialogRef<EditTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {task: Task, categories: Category[], tags: Tag[]}
    ) {
      this.form.patchValue({
        text: data.task.text,
        category: data.task.category?.id
      })
    }
  
  updatedTask(updated: Partial<Task>) {
    return {...updated, id: this.data.task.id}
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
