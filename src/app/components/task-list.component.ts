import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SubTask, Task } from '../models/task.model';

@Component({
  selector: 'app-task-list',
  template: `
  <div class="data-row">
    <span><span *ngIf="tasksToDo(tasks) as toDoCount">To do: {{toDoCount}} </span><span class="done" *ngIf="tasksDone(tasks) as doneCount">Done: {{doneCount}} </span></span>
    <a class="clear" (click)="clearFilters.next()">Clear Filters</a>
  </div>
  <hr>
  <ul>
    <li *ngFor="let task of tasks"> 
      <mat-checkbox
        [checked]="task.done"
        (change)="switchStatus(task. id, task.done)"
        [indeterminate]="task.subTasks?.length ? someComplete(task.subTasks): false">
        <div class="label-row">
        <span>{{task.text}}</span>
        <span>
            <mat-icon inline=true (click)="emitEditTask($event, task)">edit</mat-icon>
            <mat-icon color="warn" (click)="emitDeleteTask($event, task.id)" inline=true>clear</mat-icon>
        </span>
        </div>
        <div class="label-row">
        <span class="category">{{task.category?.name}}</span>
        </div>
      </mat-checkbox>
      <ul *ngIf="task.subTasks.length">
        <li
          *ngFor="let subTask of task.subTasks"
        >
          <mat-checkbox
            [checked]="subTask.done">
            {{subTask.text}}
          </mat-checkbox>
        </li>
      </ul>
    </li>
  </ul>
  `,
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent {
  @Input() tasks: Task[];
  @Output() editTask = new EventEmitter<Task>();
  @Output() swithcTaskStatus = new EventEmitter<Partial<Task>>();
  @Output() deleteTask = new EventEmitter<string>();
  @Output() clearFilters = new EventEmitter();

  switchStatus(id: string, done: boolean) {
    this.swithcTaskStatus.next({
      id,
      done: !done
    })
  }

  someComplete(subtasks: SubTask[]): boolean {
    return subtasks.filter(t => t.done).length > 0
  }

  tasksToDo(tasks: Task[]) {
    return tasks.filter(t => !t.done).length
  }
  tasksDone(tasks: Task[]) {
    return tasks.filter(t => t.done).length
  }

  emitEditTask(e: Event, task: Task) {
    e.preventDefault();
    this.editTask.next(task)
  }
  emitDeleteTask(e: Event, id: string) {
    e.preventDefault();
    this.deleteTask.next(id)
  }
}
