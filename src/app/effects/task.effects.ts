import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import * as TaskActions from '../actions/task.actions';
import { TasksService } from '../services/tasks.service';




@Injectable()
export class TaskEffects {

  loadTasks$ = createEffect(() => this.actions$.pipe(
    ofType(TaskActions.loadTasks),
    exhaustMap(() =>
      this.tasksService.getTasksList().pipe(
        map(tasks => TaskActions.loadTasksSuccess({ tasks })),
        catchError(error => of(TaskActions.loadTasksFailure({ error }))))
    )
  ));

  addTask$ = createEffect(() => this.actions$.pipe(
    ofType(TaskActions.addTask),
    map(a => a.task),
    exhaustMap((task) =>
      this.tasksService.addTask(task).pipe(
        map(task => TaskActions.addTaskSuccess({ task })),
        catchError(error => of(TaskActions.addTaskFailure({ error }))))
    )
  ));


  updateTask$ = createEffect(() => this.actions$.pipe(
    ofType(TaskActions.updateTask),
    map(a => a.task),
    exhaustMap((task) =>
      this.tasksService.updateTask(task).pipe(
        map(task => TaskActions.updateTaskSuccess({ task })),
        catchError(error => of(TaskActions.updateTaskFailure({ error }))))
    )
  ));

  deleteTask$ = createEffect(() => this.actions$.pipe(
    ofType(TaskActions.deleteTask),
    map(a => a.id),
    exhaustMap((id) =>
      this.tasksService.deleteTask(id).pipe(
        map(task => TaskActions.deleteTaskSuccess({id: task.id})),
        catchError(error => of(TaskActions.deleteTaskFailure({ error }))))
    )
  ));



  constructor(
    private actions$: Actions,
    private tasksService: TasksService
  ) { }

}
