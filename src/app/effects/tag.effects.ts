import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import * as TagActions from '../actions/tag.actions';
import { TagsService } from '../services/tags.service';




@Injectable()
export class TagEffects {

  loadTags$ = createEffect(() => this.actions$.pipe( 
    ofType(TagActions.loadTags),
    exhaustMap(() =>
      this.tagsService.getTagsList().pipe(
        map(tags => TagActions.loadTagsSuccess({ tags })),
        catchError(error => of(TagActions.loadTagsFailure({ error }))))
    )
  )
);

constructor(
  private actions$: Actions,
  private tagsService: TagsService
  ) {}

}
