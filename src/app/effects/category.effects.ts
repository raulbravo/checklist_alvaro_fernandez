import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import * as CategoryActions from '../actions/category.actions';
import { addTask } from '../actions/task.actions';
import { CategoriesService } from '../services/categories.service';




@Injectable()
export class CategoryEffects {

  loadCategories$ = createEffect(() => this.actions$.pipe( 
    ofType(CategoryActions.loadCategories),
    exhaustMap(() =>
      this.categoriesService.getCategoriesList().pipe(
        map(categories => CategoryActions.loadCategoriesSuccess({ categories })),
        catchError(error => of(CategoryActions.loadCategoriesFailure({ error }))))
    )
  )
);

constructor(
  private actions$: Actions,
  private categoriesService: CategoriesService
  ) {}

}
